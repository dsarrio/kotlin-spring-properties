package tf.virtual.kotlin.spring.properties

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import tf.virtual.ConfigurationException
import java.lang.reflect.InvocationTargetException
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.starProjectedType

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Retention(AnnotationRetention.RUNTIME)
annotation class PropertiesPrefix(val value: String)

fun prefixFromAnnotation(thisRef: Any): String? {
    return thisRef::class.findAnnotation<PropertiesPrefix>()?.value
}

fun <T: Any?> buildDelegate(thisRef: Any, property: KProperty<*>, path: String?, assertExists: Boolean, env: Environment, fnValue: (String) -> T) : ReadOnlyProperty<Any, T> {
    val prefix = prefixFromAnnotation(thisRef)
    val propertyName = arrayOf(prefix, path, property.name).filterNotNull().joinToString(".")
    if (assertExists && !env.containsProperty(propertyName)) {
        throw ConfigurationException("Property $propertyName is not set", "Define it through properties or as an environment variable")
    }
    return object: ReadOnlyProperty<Any, T> {
        override fun getValue(thisRef: Any, property: KProperty<*>) = fnValue(propertyName)
    }
}

interface PropertyDelegate<T> {
    operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadOnlyProperty<Any, T>
}

class NestedDelegate<T>(
        private val constructor: KFunction<T>,
        private val env: Environment,
        private val path: String? = null
): PropertyDelegate<T> {

    override operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadOnlyProperty<Any, T> {
        try {
            val propertyName = property.name
            val nestedPath = if (path == null) propertyName else "$path.${propertyName}"
            val nestedPropertyDelegate = constructor.call(PropertyProvider(env, nestedPath))
            return object : ReadOnlyProperty<Any, T> {
                override fun getValue(thisRef: Any, property: KProperty<*>) = nestedPropertyDelegate
            }
        } catch (invocationException: InvocationTargetException) {
            throw invocationException.targetException
        }
    }
}

@Component
class PropertyProvider(var env: Environment, var path: String? = null) {

    fun int() = IntPropertyProvider(env, path)

    fun string() = StringPropertyProvider(env, path)

    final inline fun <reified T> nested(): NestedDelegate<T> {
        val constructors = T::class.constructors
        if (constructors.size != 1) {
            throw RuntimeException("Class ${T::class.qualifiedName} must only have 1 constructor taking a ${PropertyProvider::class.simpleName} parameter")
        }

        val constructor = constructors.first()
        val parameters = constructor.parameters
        if (parameters.size != 1) {
            throw RuntimeException("Class ${T::class.qualifiedName} must only have 1 constructor taking a ${PropertyProvider::class.simpleName} parameter")
        }

        val paramType = parameters[0].type
        if (PropertyProvider::class.starProjectedType != paramType) {
            throw RuntimeException("Class ${T::class.qualifiedName} must only have 1 constructor taking a ${PropertyProvider::class.simpleName} parameter")
        }

        return NestedDelegate(constructor, env, path)
    }
}
