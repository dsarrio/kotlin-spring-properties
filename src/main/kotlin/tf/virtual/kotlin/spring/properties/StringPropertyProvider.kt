package tf.virtual.kotlin.spring.properties

import org.springframework.core.env.Environment
import tf.virtual.ConfigurationException
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty


abstract class StringPropertyProviderBase<T>: PropertyDelegate<T> {

    private var allowEmpty = true

    fun notEmpty(): StringPropertyProviderBase<T> {
        allowEmpty = false
        return this
    }

    fun validateValue(property: KProperty<*>, name: String, value: String) {
        if (!allowEmpty && value.isEmpty()) {
            throw ConfigurationException("Property $name must not be empty", "Ensure configuration is correct or correctly loaded")
        }
    }

}

class StringPropertyProvider(private var env: Environment, private var path: String?): StringPropertyProviderBase<String?>() {

    fun required() = RequiredStringPropertyProvider(env, path)

    override operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadOnlyProperty<Any, String?> {
        return buildDelegate(thisRef, property, path, false, env) { propertyName ->
            val value = env.getProperty(propertyName)
            if (value != null) {
                validateValue(property, propertyName, value)
            }
            return@buildDelegate value
        }
    }
}

class RequiredStringPropertyProvider(public var env: Environment, public var path: String?): StringPropertyProviderBase<String>() {

    inline fun <reified T> test(thisRef: Any, property: KProperty<*>, path: String?): ReadOnlyProperty<Any, String> {
        return buildDelegate(thisRef, property, path, true, env) { propertyName ->
            val value = env.getRequiredProperty(propertyName)
            validateValue(property, propertyName, value)
            return@buildDelegate value
        }
    }

    override operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadOnlyProperty<Any, String> {
        return test<String>(thisRef, property, path)
//        return buildDelegate(thisRef, property, path, true, env) { propertyName ->
//            val value = env.getRequiredProperty(propertyName)
//            validateValue(property, propertyName, value)
//            return@buildDelegate value
//        }
    }
}
