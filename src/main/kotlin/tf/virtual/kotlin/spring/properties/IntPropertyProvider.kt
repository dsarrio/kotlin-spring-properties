package tf.virtual.kotlin.spring.properties

import org.springframework.core.env.Environment
import tf.virtual.ConfigurationException
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

abstract class IntPropertyProviderBase<T>: PropertyDelegate<T> {

    private var min: Int? = null

    private var max: Int? = null

    fun min(value: Int): IntPropertyProviderBase<T> {
        min = value
        return this
    }

    fun max(value: Int): IntPropertyProviderBase<T> {
        max = value
        return this
    }

    protected fun validateValue(property: KProperty<*>, name: String, value: Int) {
        min?.let {
            if (value < it) {
                throw ConfigurationException("Property $name must be greater than $min", "")
            }
        }

        max?.let {
            if (value > it) {
                throw ConfigurationException("Property $name must be lower than $max", "")
            }
        }
    }

}

class IntPropertyProvider(private var env: Environment, private var path: String?): IntPropertyProviderBase<Int?>() {

    fun required() = RequiredIntPropertyProvider(env, path)

    override operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadOnlyProperty<Any, Int?> {
        return buildDelegate(thisRef, property, path, false, env) { propertyName ->
            val value = env.getProperty(propertyName, Int::class.java)
            if (value != null) {
                validateValue(property, propertyName, value)
            }
            return@buildDelegate value
        }
    }
}

class RequiredIntPropertyProvider(private var env: Environment, private var path: String?): IntPropertyProviderBase<Int>() {

    override operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadOnlyProperty<Any, Int> {
        return buildDelegate(thisRef, property, path, true, env) { propertyName ->
            val value = env.getRequiredProperty(propertyName, Int::class.java)
            validateValue(property, propertyName, value)
            return@buildDelegate value
        }
    }
}
