package tf.virtual.kotlin.spring.properties

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import tf.virtual.ConfigurationException

class StringPropertyProviderTest : PropertyProviderTest() {

    @Test
    fun `test optional string properties can be loaded when defined`() {
        test("edward", propertyProvider.string())
    }

    @Test
    fun `test optional string properties are null when not defined`() {
        test(null, propertyProvider.string())
    }

    @Test
    fun `test required string properties can be loaded when defined`() {
        test("edward", propertyProvider.string().required())
    }

    @Test
    fun `test required string properties fail to load when not defined`() {
        Assertions.assertThrows(ConfigurationException::class.java) {
            test(null, propertyProvider.string().required())
        }
    }

    @Test
    fun `test empty string properties fail to load when not allowed to be empty`() {
        Assertions.assertThrows(ConfigurationException::class.java) {
            test("", propertyProvider.string().notEmpty())
        }
    }

    @Test
    fun `test string properties load when not allowed to be empty`() {
        test("john", propertyProvider.string().notEmpty())
    }

}
