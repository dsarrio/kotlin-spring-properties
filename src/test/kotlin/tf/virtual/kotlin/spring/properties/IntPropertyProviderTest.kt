package tf.virtual.kotlin.spring.properties

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import tf.virtual.ConfigurationException

class IntPropertyProviderTest : PropertyProviderTest() {

    @Test
    fun `test optional int properties can be loaded when defined`() {
        test(42, propertyProvider.int())
    }

    @Test
    fun `test optional int properties are null when not defined`() {
        test(null, propertyProvider.int())
    }

    @Test
    fun `test required int properties can be loaded when defined`() {
        test(42, propertyProvider.int().required())
    }

    @Test
    fun `test required int properties fail to load when not defined`() {
        Assertions.assertThrows(ConfigurationException::class.java) {
            test(null, propertyProvider.int().required())
        }
    }

    @Test
    fun `test int properties load when in min-max range`() {
        test(-5, propertyProvider.int().min(-5).max(12))
        test(0, propertyProvider.int().min(-5).max(12))
        test(12, propertyProvider.int().min(-5).max(12))
    }

    @Test
    fun `test int property fail to load when lower than min limit`() {
        Assertions.assertThrows(ConfigurationException::class.java) {
            test(42, propertyProvider.int().min(43))
        }
    }

    @Test
    fun `test int property fail to load when greater than max limit`() {
        Assertions.assertThrows(ConfigurationException::class.java) {
            test(42, propertyProvider.int().max(41))
        }
    }

}
