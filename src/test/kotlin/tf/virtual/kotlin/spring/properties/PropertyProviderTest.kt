package tf.virtual.kotlin.spring.properties

import org.junit.jupiter.api.BeforeEach
import org.springframework.core.env.PropertiesPropertySource
import org.springframework.core.env.StandardEnvironment
import java.util.Properties
import kotlin.reflect.KParameter
import kotlin.reflect.KProperty
import kotlin.reflect.KType
import kotlin.reflect.KTypeParameter
import kotlin.reflect.KVisibility
import kotlin.test.assertEquals

open class PropertyProviderTest {

    protected var testProperties = Properties()
    protected var testEnvironment = StandardEnvironment().apply {
        propertySources.addFirst(PropertiesPropertySource("testProperties", testProperties))
    }
    protected var propertyProvider = PropertyProvider(testEnvironment)

    @BeforeEach
    protected fun setup() {
        testProperties.clear()
    }

    protected fun <T> test(propertyValue: T?, propertyDelegate: PropertyDelegate<T>) {
        if (propertyValue != null) {
            testProperties.setProperty("prop", propertyValue.toString())
        } else {
            testProperties.remove("prop")
        }
        val property = object:KProperty<T> {
            override val name: String = "prop"
            override val annotations: List<Annotation> get() = TODO("not implemented")
            override val getter: KProperty.Getter<T> get() = TODO("not implemented")
            override val isAbstract: Boolean get() = TODO("not implemented")
            override val isConst: Boolean get() = TODO("not implemented")
            override val isFinal: Boolean get() = TODO("not implemented")
            override val isLateinit: Boolean get() = TODO("not implemented")
            override val isOpen: Boolean get() = TODO("not implemented")
            override val isSuspend: Boolean get() = TODO("not implemented")
            override val parameters: List<KParameter> get() = TODO("not implemented")
            override val returnType: KType get() = TODO("not implemented")
            override val typeParameters: List<KTypeParameter> get() = TODO("not implemented")
            override val visibility: KVisibility? get() = TODO("not implemented")
            override fun call(vararg args: Any?): T = TODO("not implemented")
            override fun callBy(args: Map<KParameter, Any?>): T = TODO("not implemented")
        }
        val delegate = propertyDelegate.provideDelegate(this, property)
        val actual: T = delegate.getValue(this, property)
        assertEquals(propertyValue, actual)
    }

}
