package tf.virtual.kotlin.spring.properties

import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

class PropertiesPrefixTest : PropertyProviderTest() {

    @PropertiesPrefix("the.prefix")
    class SubjectWithPrefix(provider: PropertyProvider) {
        val a by provider.string()
    }

    @Test
    fun `test properties prefix gets added to key names`() {
        testProperties.setProperty("the.prefix.a", "42")
        val subject = SubjectWithPrefix(propertyProvider)
        assertTrue(subject.a == "42")
    }

}