package tf.virtual

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Component
import tf.virtual.kotlin.spring.properties.PropertiesPrefix
import tf.virtual.kotlin.spring.properties.PropertyProvider

@Component
@PropertiesPrefix("job.gitlab-report")
class MyApplicationProperties(p: PropertyProvider) {
    val a by p.string().required()
}

@SpringBootApplication
class Application(private val props: MyApplicationProperties) : CommandLineRunner {
    override fun run(args: Array<String>) {
        println("calling from run")
        println(props.a)
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
