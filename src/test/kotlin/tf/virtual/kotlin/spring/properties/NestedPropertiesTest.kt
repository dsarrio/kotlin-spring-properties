package tf.virtual.kotlin.spring.properties

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import tf.virtual.ConfigurationException
import kotlin.test.assertTrue

class Child2(provider: PropertyProvider) {
    val value2 by provider.string().required()
}

class Child11(provider: PropertyProvider) {
    val value11 by provider.string()
}

@PropertiesPrefix("this.prefix.should.be.ignored")
class Child1(provider: PropertyProvider) {
    val value1 by provider.string()
    val child11 by provider.nested<Child11>()
}

@PropertiesPrefix("the.prefix")
class Parent(provider: PropertyProvider) {
    val child1: Child1 by provider.nested()
    val child2 by provider.nested<Child2>()
}

class NestedPropertiesTest : PropertyProviderTest() {

    @Test
    fun `test nested properties can be loaded`() {
        testProperties.setProperty("the.prefix.child1.value1", "1")
        testProperties.setProperty("the.prefix.child1.child11.value11", "11")
        testProperties.setProperty("the.prefix.child2.value2", "2")
        val subject = Parent(propertyProvider)
        assertTrue(subject.child1.value1 == "1")
        assertTrue(subject.child1.child11.value11 == "11")
        assertTrue(subject.child2.value2 == "2")
    }

    @Test
    fun `test nested properties fail to load when not defined`() {
        Assertions.assertThrows(ConfigurationException::class.java) {
            Parent(propertyProvider)
        }
    }

}
